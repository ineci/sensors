import unittest
from sensors import sensors


class TestSetup(unittest.TestCase):
    def testBME680(self):
        bme680 = sensors.Sensor('bme680')
        self.assertIsInstance(bme680, sensors.BME680)

    def testBME280(self):
        bme280 = sensors.Sensor('bme280')
        self.assertIsInstance(bme280, sensors.BME280)

    def testSHT31(self):
        sht31 = sensors.Sensor('sht31')
        self.assertIsInstance(sht31, sensors.SHT31)

    def dht22(self):
        dht22 = sensors.Sensor('dht22', 17)
        self.assertIsInstance(dht22, sensors.DHT22)


if __name__ == '__main__':
    unittest.main()
