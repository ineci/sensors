Download repozitorija:

1. U neki tmp folder skinuti library
`git clone https://bitbucket.org/ineci/sensors`
2. Aktivirati virutalni enviroment (google zna)
3. Instalirati paket u virtualni enviromet `pip install /path/do/foldera/sensors`


Za početak možete kopirati kod ispod:
Bitno je da se koristi python3

```
from rpi_sensors import Sensor

sensor_bme280 = Sensor('bme280')
sensor_bme680 = Sensor('bme680')
sensor_sht31 = Sensor('sht31')
# DHT22 prima broj pina 17 znaci GPIO17 (a to odgovara pinu 11 na Raspberryu)
sensor_dht22 = Sensor('dht22', 17)

d1 = sensor_bme280.read
print(type(d1), d1)
bme280_temp = d1[0] # ili d1.temperature
print(f"Samo temperatura: {bme280_temp}*C")

d2 = sensor_bme680.read
print(type(d2), d2)

d3 = sensor_sht31.read
print(type(d3), d3)

d4 = sensor_dht22.rea
print(type(d4), d4)

```

Kad imate vrijednost senzora možete dalje kaj god treba
