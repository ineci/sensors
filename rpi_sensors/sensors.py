import logging
import time
from collections import namedtuple
import RPi.GPIO as IO

from ._bme_280 import BME280
from ._bme_680 import BME680
from ._sht_31 import SHT31
from ._dht_22 import DHT22
from ._ds18b20 import DS18B20

logger = logging.getLogger(__name__)


class Multipleksor:

    def __init__(self, a, b, c):
        """
        :param a:
        :param b:
        :param c:
        """
        IO.setmode(IO.BCM)
        logger.debug("GPIO mode set to %s", IO.BCM)
        self.A, self.B, self.C = a, b, c
        logger.debug("Multipleksor pins: A=%s, B=%s, C=%s", self.A, self.B, self.C)
        IO.setup(self.A, IO.OUT)
        IO.setup(self.B, IO.OUT)
        IO.setup(self.C, IO.OUT)
        logger.debug("Pinovi postavljeni kao output!")

    @property
    def trenutni_polozaj(self):
        return 0

    def postavi_polozaj(self, channel):
        assert type(channel) is int, "Kanal mora biti integer. %s" % channel
        assert 0 >= channel <= 7, "Kanal mora biti izmedu interger 0 i 7. %s" %channel

        logger.info("Postavljam multipleksor na kanal: %s", channel)
        bin_ch_list = list(map(int, list(bin(channel)[2:].zfill(3))))
        logger.debug("Pretvorio kanal u binarnu listu [a, b, c] = %s", bin_ch_list)

        IO.output(self.A, bin_ch_list[2])
        IO.output(self.B, bin_ch_list[1])
        IO.output(self.C, bin_ch_list[0])
        logger.debug("Sleep for 0.1 sec...")
        time.sleep(0.1)
        logger.info("Multipleksor postavljen na C=%s, B=%s, A=%s", IO.input(self.C), IO.input(self.B), IO.input(self.A))


class Sensor:
    
    Data = namedtuple("Data", "temperature, humidity, pressure")
    
    def __init__(self, sensor_type, gpio=None, name=None, mp=None, mp_ch=None):
        assert type(sensor_type) is str, "Sensor_type mora biti tipa String!"
        assert type(mp) in (type(None) or Multipleksor), "MP mora biti instanca klase Multipleksor"

        logger.info("Init sensor with parameters: sensor_type=%s, gpio=%s, name=%s, mp=%s, mp_ch=%s", sensor_type,
                    gpio, name, mp, mp_ch)

        self.name = name if name else sensor_type
        self.mp = mp
        self.mp_ch = mp_ch

        if self.mp:
            logger.info("Postavljam polozaj multipleksora na %s", mp_ch)
            self.mp.postavi_polozaj(self.mp_ch)
        else:
            logger.debug("Za senzore %s nije dan multipleksor", sensor_type)

        if sensor_type == "sht31":
            logger.info("Inicijaliziram senzor %s", sensor_type)
            self.s = SHT31()
        elif sensor_type == "bme280":
            self.s = BME280()
            logger.info("Inicijaliziram senzor %s", sensor_type)
        elif sensor_type == "bme680":
            self.s = BME680()
            logger.info("Inicijaliziram senzor %s", sensor_type)
        elif sensor_type == "dht22":
            self.s = DHT22(gpio)
            logger.info("Inicijaliziram senzor %s", sensor_type)
        elif sensor_type == "ds18b20":
            self.s = DS18B20()
        else:
            logger.error("Ne postoji senzor %s", sensor_type)
            raise AttributeError("Ne postoji senzor %s" % sensor_type)

    @property
    def read(self):

        logger.debug("Prikupljanje podataka sa senzora %s", self.name)
        try:
            logger.info("Postavljam polozaj za senzor", self.name, "na kanal", self.mp_ch)
            self.mp.postavi_polozaj(self.mp_ch)
            r = self.s.read_data()

            broj_parametara = len(r)

            if broj_parametara == 1:
                return self.Data(r[0], None, None)
            if broj_parametara == 2:
                return self.Data(r[0], r[1], None)
            elif broj_parametara == 3:
                return self.Data(r[0], r[1], r[2])
            else:
                raise ValueError("Greska kod citanja sa senzora %s", self.name)
        except Exception as e:
            logger.exception(e)
            raise

