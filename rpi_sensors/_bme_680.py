import bme680


class BME680:

    def __init__(self, adr=0x77):
        self.sensor = bme680.BME680(adr)
        self.sensor.set_humidity_oversample(bme680.OS_2X)
        self.sensor.set_pressure_oversample(bme680.OS_4X)
        self.sensor.set_temperature_oversample(bme680.OS_8X)
        self.sensor.set_filter(bme680.FILTER_SIZE_3)
        self.sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)

    def read_data(self):
        if self.sensor.get_sensor_data():
            temp = self.sensor.data.temperature
            hum = self.sensor.data.humidity
            pressure = self.sensor.data.pressure
            return temp, hum, pressure
