import Adafruit_DHT


class DHT22:

    def __init__(self, pin):
        self.pin = pin
        self.sensor = Adafruit_DHT.DHT22

    def read_data(self):
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.pin)
        return (temperature, humidity)
