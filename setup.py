import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='rpi-sensors',
    version='0.2.10',
    author="Appendix",
    author_email="appendix@null.net",
    description="United library for DHT22, SHT31, BME280, BME680. Using any of the sensors in the same way",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ineci/sensors",
    packages=setuptools.find_packages(),
    include_package_data=True,
    # install_requires=required,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
