import time
from collections import namedtuple

from ._bme_280 import BME280
from ._bme_680 import BME680
from ._sht_31 import SHT31
from ._dht_22 import DHT22
from ._ds18b20 import DS18B20


class Sensor:
    
    Data = namedtuple("Data", "temperature, humidity, pressure")
    
    def __init__(self, sensor_type, gpio=None, name=None):
        if sensor_type == "sht31":
            self.s = SHT31()
        elif sensor_type == "bme280":
            self.s = BME280()
        elif sensor_type == "bme680":
            self.s = BME680()
        elif sensor_type == "dht22":
            self.s = DHT22(gpio)
        elif sensor_type == "ds18b20":
            self.s = DS18B20()

        self.name = name if name else sensor_type
    
    @property
    def read(self):
        r = self.s.read_data()
        broj_parametara = len(r)
        
        if broj_parametara == 1:
            return self.Data(r[0], None, None)
        if broj_parametara == 2:
            return self.Data(r[0], r[1], None)
        elif broj_parametara == 3:
            return self.Data(r[0], r[1], r[2])
        else:
            return None


if __name__=="__main__":
    pass
