import time
import smbus


class SHT31:
    ADDRESS = 0x44

    def __init__(self, bus=smbus.SMBus(1)):
        self.bus = bus

    def read_data(self):
        self.bus.write_i2c_block_data(self.ADDRESS, 0x2C, [0x06])
        time.sleep(0.5)
        data = self.bus.read_i2c_block_data(self.ADDRESS, 0x00, 6)
        raw_temp = data[0] * 256 + data[1]
        temp = -45 + (175 * raw_temp / 65535.0)
        hum = 100 * (data[3] * 256 + data[4]) / 65535.0

        return temp, hum