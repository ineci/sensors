import smbus
import time
from ctypes import c_short
from ctypes import c_byte
from ctypes import c_ubyte

from collections import namedtuple


def getShort(data, index):
    # return two bytes from data as a signed 16-bit value
    return c_short((data[index+1] << 8) + data[index]).value

def getUShort(data, index):
    # return two bytes from data as an unsigned 16-bit value
    return (data[index+1] << 8) + data[index]

def getChar(data,index):
    # return one byte from data as a signed char
    result = data[index]
    if result > 127:
        result -= 256
    return result

def getUChar(data,index):
    # return one byte from data as an unsigned char
    result =  data[index] & 0xFF
    return result
    
class Sensor():
    
    Data = namedtuple("Data", "temperature, humidity, pressure")
    
    def __init__(self, sensor_type, GPIO=None, name=None):
        _S = {
            'bme280': BME280,
            'bme680': BME680,
            'sht31': SHT31, 
            'dht22': DHT22(),
            'ds18b20': DS18B2
        }[sensor_type]
        self.s = _S()
        self.name = name if name else sensor_type
    
    @property
    def read(self):
        r = self.s.read_data()
        broj_parametara = len(r)
        
        if broj_parametara == 2:
            return self.Data(r[0], r[1], None)
        elif broj_parametara == 3:
            return self.Data(r[0], r[1], r[2])
        else:
            return None


class BME280:
    DEVICE = 0x76
    # Register Addresses
    REG_DATA = 0xF7
    REG_CONTROL = 0xF4
    REG_CONFIG  = 0xF5

    REG_CONTROL_HUM = 0xF2
    REG_HUM_MSB = 0xFD
    REG_HUM_LSB = 0xFE

    # Oversample setting - page 27
    OVERSAMPLE_TEMP = 2
    OVERSAMPLE_PRES = 2
    MODE = 1
    OVERSAMPLE_HUM = 2

    def __init__(self, addr=None):
        self.addr = self.DEVICE
        self.bus = smbus.SMBus(1)

    def read_data(self):
        self.bus.write_byte_data(self.addr, self.REG_CONTROL_HUM, self.OVERSAMPLE_HUM)
        
        control = self.OVERSAMPLE_TEMP<<5 | self.OVERSAMPLE_PRES<<2 | self.MODE
        self.bus.write_byte_data(self.addr, self.REG_CONTROL, control)
        # Read blocks of calibration data from EEPROM
        # See Page 22 data sheet
        cal1 = self.bus.read_i2c_block_data(self.addr, 0x88, 24)
        cal2 = self.bus.read_i2c_block_data(self.addr, 0xA1, 1)
        cal3 = self.bus.read_i2c_block_data(self.addr, 0xE1, 7)
        
        # Convert byte data to word values
        dig_T1 = getUShort(cal1, 0)
        dig_T2 = getShort(cal1, 2)
        dig_T3 = getShort(cal1, 4)
    
        dig_P1 = getUShort(cal1, 6)
        dig_P2 = getShort(cal1, 8)
        dig_P3 = getShort(cal1, 10)
        dig_P4 = getShort(cal1, 12)
        dig_P5 = getShort(cal1, 14)
        dig_P6 = getShort(cal1, 16)
        dig_P7 = getShort(cal1, 18)
        dig_P8 = getShort(cal1, 20)
        dig_P9 = getShort(cal1, 22)
    
        dig_H1 = getUChar(cal2, 0)
        dig_H2 = getShort(cal3, 0)
        dig_H3 = getUChar(cal3, 2)
    
        dig_H4 = getChar(cal3, 3)
        dig_H4 = (dig_H4 << 24) >> 20
        dig_H4 = dig_H4 | (getChar(cal3, 4) & 0x0F)
    
        dig_H5 = getChar(cal3, 5)
        dig_H5 = (dig_H5 << 24) >> 20
        dig_H5 = dig_H5 | (getUChar(cal3, 4) >> 4 & 0x0F)
    
        dig_H6 = getChar(cal3, 6)
    
        # Wait in ms (Datasheet Appendix B: Measurement time and current calculation)
        wait_time = 1.25 + (2.3 * self.OVERSAMPLE_TEMP) + ((2.3 * self.OVERSAMPLE_PRES) + 0.575) + ((2.3 * self.OVERSAMPLE_HUM)+0.575)
        time.sleep(wait_time/1000)  # Wait the required time
    
        # Read temperature/pressure/humidity
        data = self.bus.read_i2c_block_data(self.addr, self.REG_DATA, 8)
        pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
        temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
        hum_raw = (data[6] << 8) | data[7]
    
        #Refine temperature
        var1 = ((((temp_raw>>3)-(dig_T1<<1)))*(dig_T2)) >> 11
        var2 = (((((temp_raw>>4) - (dig_T1)) * ((temp_raw>>4) - (dig_T1))) >> 12) * (dig_T3)) >> 14
        t_fine = var1+var2
        temperature = float(((t_fine * 5) + 128) >> 8);
    
        # Refine pressure and adjust for temperature
        var1 = t_fine / 2.0 - 64000.0
        var2 = var1 * var1 * dig_P6 / 32768.0
        var2 = var2 + var1 * dig_P5 * 2.0
        var2 = var2 / 4.0 + dig_P4 * 65536.0
        var1 = (dig_P3 * var1 * var1 / 524288.0 + dig_P2 * var1) / 524288.0
        var1 = (1.0 + var1 / 32768.0) * dig_P1
        if var1 == 0:
            pressure=0
        else:
            pressure = 1048576.0 - pres_raw
            pressure = ((pressure - var2 / 4096.0) * 6250.0) / var1
            var1 = dig_P9 * pressure * pressure / 2147483648.0
            var2 = pressure * dig_P8 / 32768.0
            pressure = pressure + (var1 + var2 + dig_P7) / 16.0
        
        # Refine humidity
        humidity = t_fine - 76800.0
        humidity = (hum_raw - (dig_H4 * 64.0 + dig_H5 / 16384.0 * humidity)) * (dig_H2 / 65536.0 * (1.0 + dig_H6 / 67108864.0 * humidity * (1.0 + dig_H3 / 67108864.0 * humidity)))
        humidity = humidity * (1.0 - dig_H1 * humidity / 524288.0)
        if humidity > 100:
            humidity = 100
        elif humidity < 0:
            humidity = 0
    
        return (temperature/100.0,humidity,pressure/100.0)

        
import bme680
class BME680:
    
    def __init__(self, adr=0x77):
        self.sensor = bme680.BME680(adr)
        self.sensor.set_humidity_oversample(bme680.OS_2X)
        self.sensor.set_pressure_oversample(bme680.OS_4X)
        self.sensor.set_temperature_oversample(bme680.OS_8X)
        self.sensor.set_filter(bme680.FILTER_SIZE_3)
        self.sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
    
    def read_data(self):
        if self.sensor.get_sensor_data():
            temp = self.sensor.data.temperature
            hum = self.sensor.data.humidity
            pressure = self.sensor.data.pressure
            return (temp, hum, pressure)

            
class SHT31:
    ADDRESS = 0x44
    
    def __init__(self, bus=smbus.SMBus(1)):
        self.bus = bus
    
    def read_data(self):
        self.bus.write_i2c_block_data(self.ADDRESS, 0x2C, [0x06])
        time.sleep(0.5)
        data = self.bus.read_i2c_block_data(self.ADDRESS, 0x00, 6)
        raw_temp = data[0] * 256 + data[1]
        temp = -45 + (175 * raw_temp / 65535.0)
        hum = 100 * (data[3] * 256 + data[4]) / 65535.0
        
        return (temp, hum) 


import Adafruit_DHT
class DHT22:
    
    def __init__(self, pin):
        self.pin = pin
        self.sensor = Adafruit_DHT.DHT22
    
    def read_data(self):
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.pin)
        return (temperature, humidity)


import os
import glob
class DS18B20:
    def __init__(self):
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')

        self.base_dir = '/sys/bus/w1/devices/'
        self.device_folder = glob.glob(self.base_dir + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'

    def read_temp_raw(self):
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_data(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0
            return temp_c


def test():
    import time
    s = [Sensor('bme680', name='CJMCU-680'), Sensor('bme280'), Sensor('sht31'), 
        Sensor('dht22', 16, 'dht22_1'), Sensor('dht22', 26, 'dht22_2')]
    
    while True:
        with open("sensor_compare.txt", "a") as f:
            for i in s:
                d = i.read
                f.write(f"{time.time()}, {i.name}, {d.temperature}, {d.humidity}, {d.pressure}\n")
        time.sleep(0.1)


if __name__=="__main__":
    test()
